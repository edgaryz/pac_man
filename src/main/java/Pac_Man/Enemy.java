package Pac_Man;

import java.awt.*;
import java.util.Random;

public class Enemy extends Rectangle {

    private static final long serialVersionUID = 1L;
    private int random = 0, smart = 1, find_path = 2;
    private int state = random;
    private int right = 0, left = 1, up = 2, down = 3;
    private int direction = -1;
    private Random randomGeneration;
    private int time = 0;
    private int targetTime = 60 * 4;
    private int speed = 4;
    private int lastDirection = -1;

    public Enemy(int x, int y) {
        randomGeneration = new Random();
        setBounds(x, y, 32, 32);
        direction = randomGeneration.nextInt(4);

    }

    public void tick() {

        if (state == random) {

            if (direction == right) {

                if (canMove(x + speed, y)) {
                    if (randomGeneration.nextInt(100) < 50) x += speed;
                } else {
                    direction = randomGeneration.nextInt(4);
                }

            } else if (direction == left) {
                if (canMove(x - speed, y)) {
                    if (randomGeneration.nextInt(100) < 50) x -= speed;
                } else {
                    direction = randomGeneration.nextInt(4);
                }
            } else if (direction == up) {
                if (canMove(x, y - speed)) {
                    if (randomGeneration.nextInt(100) < 50) y -= speed;
                } else {
                    direction = randomGeneration.nextInt(4);
                }
            } else if (direction == down) {
                if (canMove(x, y + speed)) {
                    if (randomGeneration.nextInt(100) < 50) y += speed;
                } else {
                    direction = randomGeneration.nextInt(4);
                }
            }

            time++;
            if (time == targetTime) {
                state = smart;
                time = 0;
            }

        } else if (state == smart) {

            //follow the player!

            boolean move = false;

            if (x < Game.player.x) {
                if (canMove(x + speed, y)) {
                    if (randomGeneration.nextInt(100) < 50) x += speed;
                    move = true;
                    lastDirection = right;
                }
            }
            if (x > Game.player.x) {
                if (canMove(x - speed, y)) {
                    if (randomGeneration.nextInt(100) < 50) x -= speed;
                    move = true;
                    lastDirection = left;
                }
            }
            if (y < Game.player.y) {
                if (canMove(x, y + speed)) {
                    if (randomGeneration.nextInt(100) < 50) y += speed;
                    move = true;
                    lastDirection = down;
                }
            }
            if (y > Game.player.y) {
                if (canMove(x, y - speed)) {
                    if (randomGeneration.nextInt(100) < 50) y -= speed;
                    move = true;
                    lastDirection = up;
                }
            }

            if (x == Game.player.x && y == Game.player.y) move = true;

            if (!move) {
                state = find_path;
            }

            time++;
            if (time == targetTime) {
                state = random;
                time = 0;
            }

        } else if (state == find_path) {

            if (lastDirection == right) {
                if (y < Game.player.y) {
                    if (canMove(x, y + speed)) {
                        if (randomGeneration.nextInt(100) < 50) y += speed;
                        state = smart;
                    }
                } else {
                    if (canMove(x, y - speed)) {
                        if (randomGeneration.nextInt(100) < 50) y -= speed;
                        state = smart;
                    }
                }
                if (canMove(x + speed, y)) {
                    if (randomGeneration.nextInt(100) < 50) x += speed;
                }


            } else if (lastDirection == left) {
                if (y < Game.player.y) {
                    if (canMove(x, y + speed)) {
                        if (randomGeneration.nextInt(100) < 50) y += speed;
                        state = smart;
                    }
                } else {
                    if (canMove(x, y - speed)) {
                        if (randomGeneration.nextInt(100) < 50) y -= speed;
                        state = smart;
                    }
                }
                if (canMove(x - speed, y)) {
                    if (randomGeneration.nextInt(100) < 50) x -= speed;
                }


            } else if (lastDirection == up) {
                if (x < Game.player.x) {
                    if (canMove(x + speed, y)) {
                        if (randomGeneration.nextInt(100) < 50) x += speed;
                        state = smart;
                    }
                } else {
                    if (canMove(x - speed, y)) {
                        if (randomGeneration.nextInt(100) < 50) x -= speed;
                        state = smart;
                    }
                }
                if (canMove(x, y - speed)) {
                    if (randomGeneration.nextInt(100) < 50) y -= speed;
                }


            } else if (lastDirection == down) {
                if (x < Game.player.x) {
                    if (canMove(x + speed, y)) {
                        if (randomGeneration.nextInt(100) < 50) x += speed;
                        state = smart;
                    }
                } else {
                    if (canMove(x - speed, y)) {
                        if (randomGeneration.nextInt(100) < 50) x -= speed;
                        state = smart;
                    }
                }
                if (canMove(x, y + speed)) {
                    if (randomGeneration.nextInt(100) < 50) y += speed;
                }
            }

            time++;
            if (time == targetTime) {
                state = random;
                time = 0;
            }

        }
    }


//            time++;
//            if (time == targetTime*2) {
//                state = random;
//                time = 0;
//            }


    private boolean canMove(int nextx, int nexty) {

        Rectangle bounds = new Rectangle(nextx, nexty, width, height);
        Level level = Game.level;

        for (int xx = 0; xx < level.tiles.length; xx++) {
            for (int yy = 0; yy < level.tiles[0].length; yy++) {
                if (level.tiles[xx][yy] != null) {
                    if (bounds.intersects(level.tiles[xx][yy])) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void render(Graphics g) {
        g.drawImage(Texture.ghost[0], x, y, width, height, null);

    }
}

